﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryAPI.Migrations
{
    public partial class PopulaeUserBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "UserBooks",
                columns: new[] { "UserBookId", "BookId", "DateTime", "PaidPenalty", "Penalty", "UserId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 4, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 0m, 1 },
                    { 2, 2, new DateTime(2020, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 0m, 1 },
                    { 3, 4, new DateTime(2020, 3, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 16m, 3 },
                    { 4, 7, new DateTime(2020, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 18m, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserBooks",
                keyColumn: "UserBookId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserBooks",
                keyColumn: "UserBookId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "UserBooks",
                keyColumn: "UserBookId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "UserBooks",
                keyColumn: "UserBookId",
                keyValue: 4);
        }
    }
}
