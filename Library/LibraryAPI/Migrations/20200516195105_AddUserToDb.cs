﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryAPI.Migrations
{
    public partial class AddUserToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBooks_Readers_UserId",
                table: "UserBooks");

            migrationBuilder.DropTable(
                name: "Librarians");

            migrationBuilder.DropTable(
                name: "Readers");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    CNP = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    isLibrarian = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CNP", "Name", "Password", "UserName", "isLibrarian" },
                values: new object[,]
                {
                    { 1, "2980505125467", "Alexandra Tantau", "bb22ww", "ale22", 0 },
                    { 2, "1980505125678", "Bogdan Dobai", "bb22ww", "dobai11", 0 },
                    { 3, "1980403125467", "Andrei Tanca", "bb22ww", "tanka55", 0 },
                    { 4, "2980505125682", "Anamaria Sfirlea", "bb22ww", "ana88", 0 },
                    { 5, "1940305125467", "Ion Mantea", "bb22ww", "ion06", 0 },
                    { 6, "1820505125467", "Vasile Mutu", "bb22ww", "vasile", 0 },
                    { 7, "2860505125167", "Carla Sirta", "bb22ww", "carla43", 0 },
                    { 8, "2880305125167", "Leila Nas", "bb22ww", "leila11", 1 },
                    { 9, "1781205125167", "Gabriel Pop", "bb22ww", "gabi23", 1 },
                    { 10, "2782505125167", "Felicia Bob", "bb22ww", "felicia", 1 },
                    { 11, "1902505125347", "Mirel Dan", "bb22ww", "mirel20", 1 },
                    { 12, "1672505425167", "Raul Dorne", "bb22ww", "raul45", 1 },
                    { 13, "2762505125234", "Paula Ghint", "bb22ww", "paula67", 1 },
                    { 14, "1972505125678", "George Banel", "bb22ww", "george45", 1 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_UserBooks_Users_UserId",
                table: "UserBooks",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBooks_Users_UserId",
                table: "UserBooks");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.CreateTable(
                name: "Librarians",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CNP = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Librarians", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Readers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CNP = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Readers", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Librarians",
                columns: new[] { "Id", "CNP", "Name", "Password", "UserName" },
                values: new object[,]
                {
                    { 1, "2880305125167", "Leila Nas", "bb22ww", "leila11" },
                    { 2, "1781205125167", "Gabriel Pop", "bb22ww", "gabi23" },
                    { 3, "2782505125167", "Felicia Bob", "bb22ww", "felicia" },
                    { 4, "1902505125347", "Mirel Dan", "bb22ww", "mirel20" },
                    { 5, "1672505425167", "Raul Dorne", "bb22ww", "raul45" },
                    { 6, "2762505125234", "Paula Ghint", "bb22ww", "paula67" },
                    { 7, "1972505125678", "George Banel", "bb22ww", "george45" }
                });

            migrationBuilder.InsertData(
                table: "Readers",
                columns: new[] { "Id", "CNP", "Name", "Password", "UserName" },
                values: new object[,]
                {
                    { 1, "2980505125467", "Alexandra Tantau", "bb22ww", "ale22" },
                    { 2, "1980505125678", "Bogdan Dobai", "bb22ww", "dobai11" },
                    { 3, "1980403125467", "Andrei Tanca", "bb22ww", "tanka55" },
                    { 4, "2980505125682", "Anamaria Sfirlea", "bb22ww", "ana88" },
                    { 5, "1940305125467", "Ion Mantea", "bb22ww", "ion06" },
                    { 6, "1820505125467", "Vasile Mutu", "bb22ww", "vasile" },
                    { 7, "2860505125167", "Carla Sirta", "bb22ww", "carla43" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_UserBooks_Readers_UserId",
                table: "UserBooks",
                column: "UserId",
                principalTable: "Readers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
