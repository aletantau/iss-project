﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryAPI.Migrations
{
    public partial class CreateAndPopulateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Genres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Librarians",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    CNP = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Librarians", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Readers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    CNP = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Readers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Author = table.Column<string>(maxLength: 100, nullable: false),
                    Publication = table.Column<string>(maxLength: 50, nullable: true),
                    Year = table.Column<int>(nullable: false),
                    GenreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserBooks",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    BookId = table.Column<int>(nullable: false),
                    UserBookId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(type: "date", nullable: false),
                    Penalty = table.Column<decimal>(nullable: false),
                    PaidPenalty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBooks", x => x.UserBookId);
                    table.ForeignKey(
                        name: "FK_UserBooks_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBooks_Readers_UserId",
                        column: x => x.UserId,
                        principalTable: "Readers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Romance" },
                    { 2, "Adventure" },
                    { 3, "Fantasy" },
                    { 4, "Fiction" },
                    { 5, "Personal Growth" },
                    { 6, "Science-fiction" }
                });

            migrationBuilder.InsertData(
                table: "Librarians",
                columns: new[] { "Id", "CNP", "Name", "Password", "UserName" },
                values: new object[,]
                {
                    { 7, "1972505125678", "George Banel", "bb22ww", "george45" },
                    { 6, "2762505125234", "Paula Ghint", "bb22ww", "paula67" },
                    { 5, "1672505425167", "Raul Dorne", "bb22ww", "raul45" },
                    { 4, "1902505125347", "Mirel Dan", "bb22ww", "mirel20" },
                    { 3, "2782505125167", "Felicia Bob", "bb22ww", "felicia" },
                    { 2, "1781205125167", "Gabriel Pop", "bb22ww", "gabi23" },
                    { 1, "2880305125167", "Leila Nas", "bb22ww", "leila11" }
                });

            migrationBuilder.InsertData(
                table: "Readers",
                columns: new[] { "Id", "CNP", "Name", "Password", "UserName" },
                values: new object[,]
                {
                    { 1, "2980505125467", "Alexandra Tantau", "bb22ww", "ale22" },
                    { 2, "1980505125678", "Bogdan Dobai", "bb22ww", "dobai11" },
                    { 3, "1980403125467", "Andrei Tanca", "bb22ww", "tanka55" },
                    { 4, "2980505125682", "Anamaria Sfirlea", "bb22ww", "ana88" },
                    { 5, "1940305125467", "Ion Mantea", "bb22ww", "ion06" },
                    { 6, "1820505125467", "Vasile Mutu", "bb22ww", "vasile" },
                    { 7, "2860505125167", "Carla Sirta", "bb22ww", "carla43" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "Author", "GenreId", "Publication", "Title", "Year" },
                values: new object[,]
                {
                    { 1, "Nicholas Sparks", 1, "Grand Central Publishing", "The Notebook", 2014 },
                    { 2, "Nicholas Sparks", 1, "RAO", "The Longest Ride", 2018 },
                    { 3, "J.R.R Tolkien", 3, "Allen & Unwin ", "The Hobbit", 2012 },
                    { 4, "Delia Owens", 4, "Arthur", "Where the Crawdads sing", 2016 },
                    { 5, "Delia Owens", 4, "Arthur", "Where the Crawdads sing", 2016 },
                    { 6, "Ryan Holiday", 5, "Nautilus English Books", "Ego is the enemy", 2013 },
                    { 7, "Charles Duhigg", 5, "Wired", "The power of habit", 2015 },
                    { 8, "Ryan Holiday", 5, "Nautilus English Books", "Ego is the enemy", 2013 },
                    { 9, "Andy Weir", 6, "Nautilus English Books", "The martian", 2015 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_GenreId",
                table: "Books",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBooks_BookId",
                table: "UserBooks",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBooks_UserId",
                table: "UserBooks",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Librarians");

            migrationBuilder.DropTable(
                name: "UserBooks");

            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Readers");

            migrationBuilder.DropTable(
                name: "Genres");
        }
    }
}
