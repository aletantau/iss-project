﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryAPI.Migrations
{
    public partial class AllowNullUserBookAttributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isLibrarian",
                table: "Users",
                newName: "IsLibrarian");

            migrationBuilder.RenameColumn(
                name: "isBorrowed",
                table: "Books",
                newName: "IsBorrowed");

            migrationBuilder.AlterColumn<decimal>(
                name: "Penalty",
                table: "UserBooks",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "PaidPenalty",
                table: "UserBooks",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsLibrarian",
                table: "Users",
                newName: "isLibrarian");

            migrationBuilder.RenameColumn(
                name: "IsBorrowed",
                table: "Books",
                newName: "isBorrowed");

            migrationBuilder.AlterColumn<decimal>(
                name: "Penalty",
                table: "UserBooks",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PaidPenalty",
                table: "UserBooks",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
