﻿using AutoMapper;

namespace LibraryAPI.Profiles
{
    public class UserBookProfile : Profile
    {
        public UserBookProfile()
        {
            CreateMap<Entities.UserBook, Models.UserBookDto>();
            CreateMap<Models.UserBookForCreationDto, Entities.UserBook>().ReverseMap();

            CreateMap<Models.RequestBookDto, Entities.UserBook>().ReverseMap();
            CreateMap<Models.RequestBookDto, Entities.RequestBook>().ReverseMap();
            CreateMap<Models.UserBookForCreationDto, Entities.RequestBook>().ReverseMap();
        }
    }
}
