﻿using AutoMapper;

namespace LibraryAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<Entities.User, Models.UserDto>();
            CreateMap<Models.UserForCreationDto, Entities.User>().ReverseMap();
            CreateMap<Models.UserLoginDto, Entities.User>().ReverseMap();
        }
    }
}
