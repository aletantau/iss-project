﻿using AutoMapper;
namespace LibraryAPI.Profiles
{
    public class GenreProfile : Profile
    {
        public GenreProfile()
        {
            CreateMap<Entities.Genre, Models.GenreWithoutBooksDto>();
            CreateMap<Entities.Genre, Models.GenreDto>();
        }
    }
}
