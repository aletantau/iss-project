﻿using Microsoft.EntityFrameworkCore;

namespace LibraryAPI.Entities
{
	public class AppDbContext : DbContext
	{
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
		{
			//Database.EnsureCreated();
		}

		public DbSet<Genre> Genres { get; set; }
		public DbSet<Book> Books { get; set; }
		public DbSet<UserBook> UserBooks { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<RequestBook> Requests { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Genre>()
				.HasData(
				new Genre()
				{
					Id = 1,
					Name = "Romance"
				},
				new Genre()
				{
					Id = 2,
					Name = "Adventure"
				},
				new Genre()
				{
					Id = 3,
					Name = "Fantasy"
				},
				new Genre()
				{
					Id = 4,
					Name = "Fiction"
				},
				new Genre()
				{
					Id = 5,
					Name = "Personal Growth"
				},
				new Genre()
				{
					Id = 6,
					Name = "Science-fiction"
				}
				);

			modelBuilder.Entity<Book>()
				.HasData(
				new Book()
				{
					Id = 1,
					Title = "The Notebook",
					Author = "Nicholas Sparks",
					Publication = "Grand Central Publishing",
					Year = 2014,
					IsBorrowed = 1,
					GenreId = 1
				},
				new Book()
				{
					Id = 2,
					Title = "The Longest Ride",
					Author = "Nicholas Sparks",
					Publication = "RAO",
					Year = 2018,
					IsBorrowed = 1,
					GenreId = 1
				},
				new Book()
				{
					Id = 3,
					Title = "The Hobbit",
					Author = "J.R.R Tolkien",
					Publication = "Allen & Unwin ",
					Year = 2012,
					IsBorrowed = 0,
					GenreId = 3
				},
				new Book()
				{
					Id = 4,
					Title = "Where the Crawdads sing",
					Author = "Delia Owens",
					Publication = "Arthur",
					Year = 2016,
					IsBorrowed = 1,
					GenreId = 4
				},
				new Book()
				{
					Id = 5,
					Title = "Where the Crawdads sing",
					Author = "Delia Owens",
					Publication = "Arthur",
					Year = 2016,
					IsBorrowed = 0,
					GenreId = 4
				},
				new Book()
				{
					Id = 6,
					Title = "Ego is the enemy",
					Author = "Ryan Holiday",
					Publication = "Nautilus English Books",
					Year = 2013,
					IsBorrowed = 0,
					GenreId = 5
				},
				new Book()
				{
					Id = 7,
					Title = "The power of habit",
					Author = "Charles Duhigg",
					Publication = "Wired",
					Year = 2015,
					IsBorrowed = 1,
					GenreId = 5
				},
				new Book()
				{
					Id = 8,
					Title = "Ego is the enemy",
					Author = "Ryan Holiday",
					Publication = "Nautilus English Books",
					Year = 2013,
					IsBorrowed = 0,
					GenreId = 5
				},
				new Book()
				{
					Id = 9,
					Title = "The martian",
					Author = "Andy Weir",
					Publication = "Nautilus English Books",
					Year = 2015,
					IsBorrowed = 0,
					GenreId = 6
				}
				);

			modelBuilder.Entity<User>()
				.HasData(
				new User()
				{
					Id = 1,
					UserName = "ale22",
					Password = "bb22ww",
					CNP = "2980505125467",
					Name = "Alexandra Tantau",
					IsLibrarian=0
				},
				new User()
				{
					Id = 2,
					UserName = "dobai11",
					Password = "bb22ww",
					CNP = "1980505125678",
					Name = "Bogdan Dobai",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 3,
					UserName = "tanka55",
					Password = "bb22ww",
					CNP = "1980403125467",
					Name = "Andrei Tanca",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 4,
					UserName = "ana88",
					Password = "bb22ww",
					CNP = "2980505125682",
					Name = "Anamaria Sfirlea",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 5,
					UserName = "ion06",
					Password = "bb22ww",
					CNP = "1940305125467",
					Name = "Ion Mantea",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 6,
					UserName = "vasile",
					Password = "bb22ww",
					CNP = "1820505125467",
					Name = "Vasile Mutu",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 7,
					UserName = "carla43",
					Password = "bb22ww",
					CNP = "2860505125167",
					Name = "Carla Sirta",
					IsLibrarian = 0
				},
				new User()
				{
					Id = 8,
					UserName = "leila11",
					Password = "bb22ww",
					CNP = "2880305125167",
					Name = "Leila Nas",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 9,
					UserName = "gabi23",
					Password = "bb22ww",
					CNP = "1781205125167",
					Name = "Gabriel Pop",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 10,
					UserName = "felicia",
					Password = "bb22ww",
					CNP = "2782505125167",
					Name = "Felicia Bob",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 11,
					UserName = "mirel20",
					Password = "bb22ww",
					CNP = "1902505125347",
					Name = "Mirel Dan",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 12,
					UserName = "raul45",
					Password = "bb22ww",
					CNP = "1672505425167",
					Name = "Raul Dorne",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 13,
					UserName = "paula67",
					Password = "bb22ww",
					CNP = "2762505125234",
					Name = "Paula Ghint",
					IsLibrarian = 1
				},
				new User()
				{
					Id = 14,
					UserName = "george45",
					Password = "bb22ww",
					CNP = "1972505125678",
					Name = "George Banel",
					IsLibrarian = 1
				}
				);
			modelBuilder.Entity<UserBook>()
				.HasData(
				new UserBook
				{
					UserId = 1,
					BookId = 1,
					UserBookId = 1,
					DateTime = new System.DateTime(2020, 04, 17),
					Penalty = 0,
					PaidPenalty = 0
				},
				new UserBook
				{
					UserId = 1,
					BookId = 2,
					UserBookId = 2,
					DateTime = new System.DateTime(2020, 05, 02),
					Penalty = 0,
					PaidPenalty = 0
				},
				new UserBook
				{
					UserId = 3,
					BookId = 4,
					UserBookId = 3,
					DateTime = new System.DateTime(2020, 03, 17),
					Penalty = 16,
					PaidPenalty = 0
				}, new UserBook
				{
					UserId = 4,
					BookId = 7,
					UserBookId = 4,
					DateTime = new System.DateTime(2020, 03, 15),
					Penalty = 18,
					PaidPenalty = 1
				}
				);
			base.OnModelCreating(modelBuilder);
		}
	}
}
