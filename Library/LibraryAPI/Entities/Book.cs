﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryAPI.Entities
{
    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "A title for the book should be specified.")]
        [MaxLength(100)]
        public string Title { get; set; }

        [Required(ErrorMessage = "A name for the author should be specified.")]
        [MaxLength(100)]
        public string Author { get; set; }

        [MaxLength(50)]
        public string Publication { get; set; }

        [Range(0, 2020, ErrorMessage = "Please enter a valid year.")]
        public int Year { get; set; }
        [RegularExpression(@"^(0|1)$")]
        public int IsBorrowed { get; set; }

        [ForeignKey("GenreId")]
        public Genre Genre { get; set; }
        public int GenreId { get; set; }
    }
}
