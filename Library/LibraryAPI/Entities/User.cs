﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(50)]
        public string UserName { get; set; }

        [MaxLength(20)]
        public string Password { get; set; }

        [RegularExpression(@"^(1|2)([0-9]{12})")]
        public string CNP { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [RegularExpression(@"^(0|1)$")]
        public int IsLibrarian { get; set; }
    }
}
