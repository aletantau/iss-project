﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Entities
{
    public class RequestBook
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RequestBookId {get;set;}
        public int UserId { get; set; }
        public int BookId { get; set; }
        [Column(TypeName = "date")]
        public DateTime DateTime { get; set; }
    }
}
