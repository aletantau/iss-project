﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryAPI.Entities
{
    public class UserBook
    {
        [ForeignKey("UserId")]
        public User User { get; set; }
        public int UserId { get; set; }

        [ForeignKey("BookId")]
        public Book Book { get; set; }
        public int BookId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserBookId { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateTime { get; set; }

        [RegularExpression(@"^(\d+).([0-9]{2})$")]
        public decimal? Penalty { get; set; }

        [RegularExpression(@"^(0|1)$")]
        public int? PaidPenalty { get; set; }
    }
}
