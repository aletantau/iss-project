﻿namespace LibraryAPI.Models
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CNP { get; set; }
        public string Name { get; set; }
        public bool IsLibrarian { get; set; }
    }
}
