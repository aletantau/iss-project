﻿namespace LibraryAPI.Models
{
    public class BookForCreationDto
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public string Publication { get; set; }

        public int Year { get; set; }

        public bool isBorrowed { get; set; }
    }
}
