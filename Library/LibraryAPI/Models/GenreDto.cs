﻿using System.Collections.Generic;

namespace LibraryAPI.Models
{
    public class GenreDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //This is necesary if we want to filter all the books by a specific genre 
        int NumberOfBooks => Books.Count;
        public ICollection<BookDto> Books { get; set; } = new List<BookDto>();
    }
}
