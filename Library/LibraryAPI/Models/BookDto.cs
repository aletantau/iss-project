﻿namespace LibraryAPI.Models
{
    public class BookDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Publication { get; set; }

        public int Year { get; set; }

        public bool IsBorrowed { get; set; }
    }
}
