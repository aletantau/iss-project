﻿using System;

namespace LibraryAPI.Models
{
    public class UserBookForCreationDto
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
        public DateTime DateTime { get; set; }
    }
}
