﻿using System;

namespace LibraryAPI.Models
{
    public class UserBookDto
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
        public int UserBookId { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Penalty { get; set; }
        public bool PaidPenalty { get; set; }
    }
}
