﻿namespace LibraryAPI.Models
{
    public class GenreWithoutBooksDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
