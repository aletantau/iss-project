﻿namespace LibraryAPI.Models
{
    public class UserForCreationDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CNP { get; set; }
        public string Name { get; set; }
        public bool IsLibrarian { get; set; }
    }
}
