﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{
    public class RequestBookDto
    {
        public int RequestBookId { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public DateTime DateTime { get; set; }
    }
}
