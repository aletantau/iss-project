﻿using AutoMapper;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace LibraryAPI.Controllers
{
    [ApiController]
    [Route("api/books")]
    public class BooksController : ControllerBase
    {
        private readonly ILibraryRepository libraryRepository;
        private readonly IMapper mapper;

        public BooksController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IActionResult GetAllBooks()
        {
            var bookEntities = libraryRepository.GetAllBooks();
            return Ok(mapper.Map<IEnumerable<BookDto>>(bookEntities));
        }

        [HttpGet("{genreId}")]
        public IActionResult GetBooks(int genreId)
        {
            var books = libraryRepository.GetBooks(genreId);
            return Ok(mapper.Map<IEnumerable<BookDto>>(books));
        }

        [HttpGet("{bookId}", Name = "GetBook")]
        public IActionResult GetBookForId(int bookId)
        {
            var book = libraryRepository.GetBook(bookId);
            if (book == null)
            {
                return NotFound();
            }
            return Ok(mapper.Map<BookDto>(book));
        }

        [HttpPost("withgenreid/{genreId}")]
        public IActionResult CreateBook(int genreId, BookForCreationDto book)
        {
            var finalBook = mapper.Map<Entities.Book>(book);
            libraryRepository.AddBook(genreId, finalBook);
            libraryRepository.Save();

            var createdBookToReturn = mapper.Map<Models.BookDto>(finalBook);
            return CreatedAtRoute("GetBook", new { bookId = createdBookToReturn.Id }, createdBookToReturn);
        }

        [HttpDelete("{bookId}")]
        public IActionResult DeleteBook(int bookId)
        {
            var bookEntity = libraryRepository.GetBook(bookId);
            if (bookEntity == null)
            {
                return NotFound();
            }
            libraryRepository.DeleteBook(bookEntity);
            libraryRepository.Save();

            return NoContent();
        }


        //TODO: Update Book
    }
}
