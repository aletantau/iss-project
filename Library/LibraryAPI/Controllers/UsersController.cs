﻿using AutoMapper;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly ILibraryRepository libraryRepository;
        private readonly IMapper mapper;

        public UsersController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        }

        [HttpGet("{userId}", Name = "GetUser")]
        public IActionResult GetUserForId(int userId)
        {
            var user = libraryRepository.GetUserForId(userId);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(mapper.Map<UserDto>(user));
        }

        [HttpPost("register")]
        public IActionResult CreateUser(UserForCreationDto user)
        {
            var finalUser = mapper.Map<Entities.User>(user);
            libraryRepository.AddUser(finalUser);
            libraryRepository.Save();
            var createdUserToReturn = mapper.Map<Models.UserDto>(finalUser);
            return CreatedAtRoute("GetUser", new { userId=createdUserToReturn.Id }, createdUserToReturn);
        }

        [HttpPost("login")]
        public IActionResult LoginUser(UserLoginDto userLogin)
        {
            var finalUser = libraryRepository.LoginUser(userLogin.Username, userLogin.Password);
            if(finalUser==null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<UserDto>(finalUser));
        }

    }
}
