﻿//using AutoMapper;
//using LibraryAPI.Models;
//using LibraryAPI.Services;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;

//namespace LibraryAPI.Controllers
//{
//    [ApiController]
//    [Route("api/readers")]
//    public class ReadersController : ControllerBase
//    {
//        private readonly ILibraryRepository libraryRepository;
//        private readonly IMapper mapper;

//        public ReadersController(ILibraryRepository libraryRepository, IMapper mapper)
//        {
//            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
//            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
//        }

//        [HttpGet]
//        public IActionResult GetReaders()
//        {
//            var readers = libraryRepository.GetReaders();
//            return Ok(mapper.Map<IEnumerable<ReaderDto>>(readers));
//        }

//        [HttpGet("{readerId}", Name = "GetReader")]
//        public IActionResult GetReaderForId(int readerId)
//        {
//            var reader = libraryRepository.GetReaderForId(readerId);
//            if (reader == null)
//            {
//                return NotFound();
//            }
//            return Ok(mapper.Map<ReaderDto>(reader));
//        }

//        public IActionResult CreateReader(int genreId, ReaderForCreationDto reader)
//        {
//            var finalReader = mapper.Map<Entities.Reader>(reader);
//            libraryRepository.AddReader(finalReader);
//            libraryRepository.Save();
//            var createdReaderToReturn = mapper.Map<Models.ReaderDto>(finalReader);
//            return CreatedAtRoute("GetReader", new { readerId = createdReaderToReturn.Id }, createdReaderToReturn);
//        }

//        public IActionResult DeleteReader(int readerId)
//        {
//            var readerEntity = libraryRepository.GetReaderForId(readerId);
//            if (readerEntity == null)
//            {
//                return NotFound();
//            }
//            libraryRepository.DeleteReader(readerEntity);
//            libraryRepository.Save();

//            return NoContent();
//        }


//        //TODO: Update Reader
//    }
//}
