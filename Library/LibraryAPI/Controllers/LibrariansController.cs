﻿//using AutoMapper;
//using LibraryAPI.Models;
//using LibraryAPI.Services;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;

//namespace LibraryAPI.Controllers
//{
//    [ApiController]
//    [Route("api/librarians")]
//    public class LibrariansController : ControllerBase
//    {
//        private readonly ILibraryRepository libraryRepository;
//        private readonly IMapper mapper;

//        public LibrariansController(ILibraryRepository libraryRepository, IMapper mapper)
//        {
//            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
//            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
//        }

//        [HttpGet]
//        public IActionResult GetLibrarians()
//        {
//            var librarians = libraryRepository.GetLibrarians();
//            return Ok(mapper.Map<IEnumerable<LibrarianDto>>(librarians));
//        }

//        [HttpGet("{librarianId}", Name = "GetLibrarian")]
//        public IActionResult GetLibrarianForId(int librarianId)
//        {
//            var librarian = libraryRepository.GetLibrarianForId(librarianId);
//            if (librarian == null)
//            {
//                return NotFound();
//            }
//            return Ok(mapper.Map<LibrarianDto>(librarian));
//        }

//        public IActionResult CreateLibrarian(int librarianId, LibrarianForCreationDto librarian)
//        {
//            var finalLibrarian = mapper.Map<Entities.Librarian>(librarian);
//            libraryRepository.AddLibrarian(finalLibrarian);
//            libraryRepository.Save();
//            var createdLibrarianToReturn = mapper.Map<Models.LibrarianDto>(finalLibrarian);
//            return CreatedAtRoute("GetLibrarian", new { librarianId = createdLibrarianToReturn.Id }, createdLibrarianToReturn);
//        }

//        public IActionResult DeleteLibrarian(int librarianId)
//        {
//            var librarianEntity = libraryRepository.GetLibrarianForId(librarianId);
//            if (librarianEntity == null)
//            {
//                return NotFound();
//            }
//            libraryRepository.DeleteLibrarian(librarianEntity);
//            libraryRepository.Save();

//            return NoContent();
//        }


//        //TODO: Update Librarian
//    }
//}
