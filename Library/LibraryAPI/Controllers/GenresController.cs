﻿using AutoMapper;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace LibraryAPI.Controllers
{
    [ApiController]
    [Route("api/genres")]
    public class GenresController : ControllerBase
    {
        private readonly ILibraryRepository libraryRepository;
        private readonly IMapper mapper;

        public GenresController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IActionResult GetGenres()
        {
            var genreEntities = libraryRepository.GetGenres();

            return Ok(mapper.Map<IEnumerable<GenreWithoutBooksDto>>(genreEntities));
        }

        [HttpGet("{id}")]
        public IActionResult GetGenre(int id, bool includeBooks = false)
        {
            var genre = libraryRepository.GetGenre(id, includeBooks);

            if (genre == null)
            {
                return NotFound();
            }

            if (includeBooks)
            {
                return Ok(mapper.Map<BookDto>(genre));
            }

            return Ok(mapper.Map<GenreWithoutBooksDto>(genre));
        }
    }
}
