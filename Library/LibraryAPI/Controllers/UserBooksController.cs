﻿using AutoMapper;
using LibraryAPI.Entities;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LibraryAPI.Controllers
{
    [ApiController]
    [Route("api/userbooks")]
    public class UserBooksController : ControllerBase
    {
        private readonly ILibraryRepository libraryRepository;
        private readonly IMapper mapper;

        public UserBooksController(ILibraryRepository libraryRepository, IMapper mapper)
        {
            this.libraryRepository = libraryRepository ?? throw new ArgumentNullException(nameof(libraryRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetUsersBooks")]
        public IActionResult GetUsersBooks()
        {
            var usersBooks = libraryRepository.GetUsersBooks();
            return Ok(mapper.Map<IEnumerable<UserBookDto>>(usersBooks));
        }

        [HttpGet("books/{userId}")]
        public IActionResult GetUserBooks(int userId)
        {
            var books = libraryRepository.GetUserBooks(userId);
            return Ok(mapper.Map<IEnumerable<BookDto>>(books));
        }

        [HttpGet("reader/{bookId}")]
        public IActionResult GetReader(int bookId)
        {
            var reader = libraryRepository.GetReader(bookId);
            return Ok(mapper.Map<ReaderDto>(reader));
        }

        [HttpGet("{bookId}")]
        public IActionResult GetUserBook(int bookId)
        {
            var userBook = libraryRepository.GetUserBook(bookId);
            return Ok(mapper.Map<UserBookDto>(userBook));
        }

        [HttpGet("allrequests")]
        public IActionResult GetAllRequests()
        {
            var requestsEntities = libraryRepository.AllRequests();
            return Ok(mapper.Map<IEnumerable<RequestBookDto>>(requestsEntities));
        }

        [HttpPost("{librarianId}/acceptRequest")]
        public IActionResult BorrowedBook(int librarianId, UserBookForCreationDto userBook)
        {
            var entityRequest = mapper.Map<Entities.RequestBook>(userBook);
            var finalUserBook = mapper.Map<Entities.UserBook>(userBook);

            //verify if the user that accepts the request is librarian
            bool isLibrarian = libraryRepository.IsLibrarian(librarianId);
            if (isLibrarian)
            {
                    //Add USERBOOK in UserBook table
                    libraryRepository.borrowedBook(finalUserBook);

                    //Mark the isBorrowed field from Book Entity
                    //call the update partially, book method
                    var bookEntity = libraryRepository.GetBook(finalUserBook.BookId);
                    bookEntity.IsBorrowed = 1;
                    var bookToPatch = mapper.Map<BookForCreationDto>(bookEntity);
                    mapper.Map(bookToPatch, bookEntity);
                    libraryRepository.UpdateBook(bookEntity.GenreId, bookEntity);
            }

            libraryRepository.Save();
            return Ok();
        }

        [HttpDelete("{librarianId}/deleteRequest")]
        public IActionResult DeleteRequestBook(int librarianId, RequestBookDto userBook)
        {
            var entityRequest = mapper.Map<Entities.RequestBook>(userBook);
            //verify if the user that deletes the request is librarian
            bool isLibrarian = libraryRepository.IsLibrarian(librarianId);
            if (isLibrarian)
            {
                    //remove UserBook from table RequestBook
                    libraryRepository.DeleteRequest(entityRequest);
                    libraryRepository.Save();

            }
            return Ok();
        }

        [HttpPost("request")]
        public IActionResult RequestBook(RequestBookDto userBook)
        {
            var entityRequest = mapper.Map<Entities.RequestBook>(userBook);
            //verify for the user is reader
            bool isLibrarian = libraryRepository.IsLibrarian(userBook.UserId);
            if (isLibrarian == false)
            {
                //Add USERBOOK to request table
                libraryRepository.AddRequestBook(entityRequest);
                libraryRepository.Save();
            }
            return Ok();
        }


        [HttpDelete("{bookId}")]
        public IActionResult DeleteUserBook(int bookId)
        {
            var userBookEntity = libraryRepository.GetUserBook(bookId);
            if (userBookEntity == null)
            {
                return NotFound();
            }
            libraryRepository.DeleteUserBook(userBookEntity);
            var bookEntity = libraryRepository.GetBook(bookId);
            bookEntity.IsBorrowed = 0;
            libraryRepository.Save();

            return NoContent();
        }
    }
}