﻿using LibraryAPI.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryAPI.Services
{
    public class LibraryRepository : ILibraryRepository
    {
        private readonly AppDbContext context;

        public LibraryRepository(AppDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        //GENRE

        public IEnumerable<Genre> GetGenres()
        {
            return context.Genres.OrderBy(g => g.Name).ToList();
        }

        public Genre GetGenre(int genreId, bool includeBooks)
        {
            if (includeBooks)
            {
                return context.Genres.Include(g => g.Books).Where(g => g.Id == genreId).FirstOrDefault();
            }

            return context.Genres.Where(g => g.Id == genreId).FirstOrDefault();
        }


        //BOOK
        public IEnumerable<Book> GetAllBooks()
        {
            return context.Books.ToList();
        }

        public IEnumerable<Book> GetBooks(int genreId)
        {
            return context.Books.Where(b => b.GenreId == genreId).ToList();
        }

        public Book GetBook(int bookId)
        {
            return context.Books.Where(b => b.Id == bookId).FirstOrDefault();
        }

        public void AddBook(int genreId, Book book)
        {
            var genre = GetGenre(genreId, false);
            genre.Books.Add(book);
        }

        public void UpdateBook(int genreId, Book book)
        {

        }

        public void DeleteBook(Book book)
        {
            context.Books.Remove(book);
        }


        //USER BOOK

        public void borrowedBook(UserBook userBook)
        {
            context.UserBooks.Add(userBook);
        }

        public int calculatePenalty(DateTime borrowedDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserBook> GetUsersBooks()
        {
            return context.UserBooks.ToList();
        }

        public IEnumerable<Book> GetUserBooks(int userId)
        {
            List<Book> books = new List<Book>();
            var userBooks = context.UserBooks.Where(ub => ub.UserId == userId).ToList();
            foreach (var userBook in userBooks)
            {
                books.Add(context.Books.Find(userBook.BookId));
            }

            return books;
        }

        public UserBook GetUserBook(int bookId)
        {
            return context.UserBooks.Where(ub => ub.BookId == bookId).FirstOrDefault();
        }

        public void UpdateUserBook(int userBookId, UserBook userBook)
        {
            throw new NotImplementedException();
        }

        public void DeleteUserBook(UserBook userBook)
        {
            context.UserBooks.Remove(userBook);
        }

        public User GetReader(int bookId)
        {
            var userBook = context.UserBooks.Where(ub => ub.BookId == bookId).FirstOrDefault();
            return context.Users.Where(r => r.Id == userBook.UserId).FirstOrDefault();
        }

        //SAVE
        public bool Save()
        {
            return context.SaveChanges() >= 0;
        }

        //USER
        public User GetUserForId(int userId)
        {
            return context.Users.Where(r => r.Id == userId).FirstOrDefault();
        }

        public void AddUser(User user)
        {
            context.Users.Add(user);
        }

        public User LoginUser(string username, string password)
        {
            return context.Users.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();
        }

        public bool IsLibrarian(int userId)
        {
            bool result = false;
            var user = context.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (user.IsLibrarian == 1)
            {
                result = true;
            }
            return result;
        }

        //REQUEST BOOK
        public void AddRequestBook(RequestBook requestBook)
        {
            context.Requests.Add(requestBook);
        }

        public void DeleteRequest(RequestBook requestBook)
        {
            context.Requests.Remove(requestBook);
        }

        public bool ExistsRequest(RequestBook requestBook)
        {
            return context.Requests.Where(r => r.RequestBookId == requestBook.RequestBookId).Any();
        }

        public IEnumerable<RequestBook> AllRequests()
        {
            return context.Requests.ToList();
        }
            
    }
}
