﻿using LibraryAPI.Entities;
using System;
using System.Collections.Generic;

namespace LibraryAPI.Services
{
    public interface ILibraryRepository
    {
        //GENRE
        IEnumerable<Genre> GetGenres();

        Genre GetGenre(int genreId, bool includeBooks);

        //BOOK
        IEnumerable<Book> GetAllBooks();
        IEnumerable<Book> GetBooks(int genreId);

        Book GetBook(int bookId);

        void AddBook(int genreId, Book book);

        void UpdateBook(int genreId, Book book);

        void DeleteBook(Book book);


        //USER
        User GetUserForId(int userId);

        void AddUser(User user);

        User LoginUser(string username, string password);

       
        //USER BOOK
        IEnumerable<UserBook> GetUsersBooks();
        IEnumerable<Book> GetUserBooks(int userId);
        User GetReader(int bookId);
        UserBook GetUserBook(int bookId);
        void borrowedBook(UserBook userBook);
        void UpdateUserBook(int userBookId, UserBook userBook);
        void DeleteUserBook(UserBook userBook);
        int calculatePenalty(DateTime borrowedDate);

        //Request Book
        void AddRequestBook(RequestBook requestBook);
        void DeleteRequest(RequestBook requestBook);
        bool ExistsRequest(RequestBook requestBook);
        IEnumerable<RequestBook> AllRequests();

        //SAVE
        bool Save();

        bool IsLibrarian(int userId);
    }

}
